import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Messages.ClientCatchUp;
import Messages.ClientMessage;
import Util.Config;
import Util.Util;
import Util.Value;

/**
 * @author chris
 *
 */
public class Client
{

	private static Scanner sc;

	public static List<Value> memory = new ArrayList<>();

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException
	{
		// setup system for IPv4
		System.setProperty("java.net.preferIPv4Stack", "true");

		// read configs
		Config config = Util.getConfig(args);
		int id = config.id;
		String ip = config.client_ip;
		int port = config.client_port;

		// sequence number for sent values, to differentiate them
		int seq_num = 0;

		// set uo multicast socket
		MulticastSocket s = new MulticastSocket(port);
		s.joinGroup(InetAddress.getByName(ip));
		s.setSendBufferSize(s.getSendBufferSize() * config.bufferMultiplier);

		Runnable r = new Runnable()
		{

			@Override
			public void run()
			{
				Object o = null;
				try
				{
					o = Util.recieve(s, ip, port);
				} catch (ClassNotFoundException e)
				{
					e.printStackTrace();
				} catch (IOException e)
				{
					e.printStackTrace();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}

				/**
				 * 
				 * count majority of 1B
				 * 
				 */
				if (ClientCatchUp.class.isInstance(o))
				{
					ClientCatchUp m = (ClientCatchUp) o;
					int i = m.from;
					int count = 0;
					Value v = memory.get(i);
					// send only 10. If more needed the learner will ask again
					while (v != null && count < config.catchUpBatchsize)
					{
						ClientMessage ans = new ClientMessage(v);
						try
						{
							Util.send(ans, s, config.proposer_ip, config.proposer_port);
						} catch (IOException e)
						{
							e.printStackTrace();
						} catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						i++;
						count++;
						v = memory.get(i);
					}
				}

			}
		};

		if (config.proposerCatchUp)
		{
			new Thread(r).start();
		}

		// read System in for values
		sc = new Scanner(System.in);

		// main loop
		while (sc.hasNextLine())
		{
			String value = sc.nextLine();

			ClientMessage m = new ClientMessage(new Value(value, id, seq_num));
			memory.add(m.value);
			Util.send(m, s, config.proposer_ip, config.proposer_port);
			seq_num++;
		}

	}
}
