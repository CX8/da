package Messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import Util.Value;

public class Message2A extends Message implements Serializable
{

	private static final long serialVersionUID = 1L;
	public List<Value> decision = new ArrayList<>();
	public int fromId;

	public int rnd;
	public int paxos_rnd;

	public Message2A(List<Value> decision, int fromId, int rnd, int paxos_rnd)
	{
		if (decision != null)
		{
			this.decision = decision;
		}

		this.fromId = fromId;
		this.rnd = rnd;
		this.paxos_rnd = paxos_rnd;
	}
}
