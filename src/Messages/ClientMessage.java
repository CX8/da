package Messages;

import java.io.Serializable;

import Util.Value;

public class ClientMessage extends Message implements Serializable
{

	private static final long serialVersionUID = 1L;
	public Value value;

	public ClientMessage(Value value)
	{
		this.value = value;
	}

}
