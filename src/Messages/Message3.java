package Messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import Util.Value;

public class Message3 extends Message implements Serializable// , Comparable<Message3>
{

	private static final long serialVersionUID = 1L;
	public List<Value> decision = new ArrayList<>();
	public int fromId;

	public int rnd;
	public int paxos_rnd;

	public Message3(List<Value> decision, int fromId, int rnd, int paxos_rnd)
	{
		if (decision != null)
		{
			this.decision = decision;
		}
		this.fromId = fromId;
		this.rnd = rnd;
		this.paxos_rnd = paxos_rnd;
	}

	// To enable sorting, maybe not needed
	// @Override
	// public int compareTo(Message3 o)
	// {
	//
	// if (this.paxos_rnd > o.paxos_rnd)
	// return 1;
	// if (this.paxos_rnd == o.paxos_rnd)
	// return 10;
	// return -1;
	// }
	//
	// public static Comparator<Message3> FruitNameComparator = new
	// Comparator<Message3>()
	// {
	//
	// public int compare(Message3 m1, Message3 m2)
	// {
	// return m1.compareTo(m2);
	// }
	// };

}
