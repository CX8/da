package Messages;

import java.io.Serializable;

public class Message1A extends Message implements Serializable
{

	private static final long serialVersionUID = 1L;
	public int fromId;
	public int rnd;
	public int paxos_rnd;

	public Message1A(int fromId, int rnd, int paxos_rnd)
	{
		super();
		this.fromId = fromId;
		this.rnd = rnd;
		this.paxos_rnd = paxos_rnd;
	}

}
