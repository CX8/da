package Messages;

import java.io.Serializable;

public class WrongPaxosRnd extends Message implements Serializable
{

	private static final long serialVersionUID = 1L;
	public int fromId;
	public int paxos_rnd;
	public int rnd;

	public WrongPaxosRnd(int fromId, int paxos_rnd, int rnd)
	{
		this.fromId = fromId;
		this.paxos_rnd = paxos_rnd;
		this.rnd = rnd;
	}
}
