package Messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import Util.Value;

public class Message1B extends Message implements Serializable
{

	private static final long serialVersionUID = 1L;
	public List<Value> values = new ArrayList<>();
	public int fromId;
	public int toId;
	public int rnd;
	public int vrnd;
	public int paxos_rnd;

	public Message1B(List<Value> values, int fromId, int toId, int rnd, int vrnd, int paxos_rnd)
	{
		super();
		if (values != null)
		{
			this.values = values;
		}
		this.fromId = fromId;
		this.toId = toId;
		this.rnd = rnd;
		this.vrnd = vrnd;
		this.paxos_rnd = paxos_rnd;
	}

}
