package Messages;

import java.io.Serializable;

public class MessageCatchUp extends Message implements Serializable
{

	private static final long serialVersionUID = 1L;
	public int fromId;
	public int paxos_rnd;

	public MessageCatchUp(int fromId, int paxos_rnd)
	{
		this.fromId = fromId;
		this.paxos_rnd = paxos_rnd;
	}
}
