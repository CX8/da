package Util;

import java.io.Serializable;

public class Value implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String value;
	public int clientId;
	public int seq_num;

	public Value(String value, int clientId, int seq_num)
	{
		super();
		this.value = value;
		this.clientId = clientId;
		this.seq_num = seq_num;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + clientId;
		result = prime * result + seq_num;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Value other = (Value) obj;
		if (clientId != other.clientId)
			return false;
		if (seq_num != other.seq_num)
			return false;
		if (value == null)
		{
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
