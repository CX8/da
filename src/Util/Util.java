package Util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import Messages.Message;

public class Util
{

	public static Config config;

	/**
	 * 
	 * @param args
	 * @return
	 */
	public static Config getConfig(String[] args)
	{
		if (args.length < 2)
		{
			System.err.println("two parameters needed, id and config file");
		}

		BufferedReader br = null;
		FileReader fr = null;
		try
		{
			fr = new FileReader(args[1]);
			br = new BufferedReader(fr);

			String sCurrentLine;
			config = new Config();
			config.id = Integer.parseInt(args[0]);
			while ((sCurrentLine = br.readLine()) != null)
			{
				String[] data = sCurrentLine.split(" +");
				if (data.length == 0)
				{
					System.err.println("config file use wrong format1");
				} else if (data[0].equals("clients") && data.length == 3)
				{
					config.client_ip = data[1];
					config.client_port = Integer.parseInt(data[2]);
				} else if (data[0].equals("proposers") && data.length == 3)
				{
					config.proposer_ip = data[1];
					config.proposer_port = Integer.parseInt(data[2]);
				} else if (data[0].equals("acceptors") && data.length == 3)
				{
					config.acceptors_ip = data[1];
					config.acceptors_port = Integer.parseInt(data[2]);
				} else if (data[0].equals("learners") && data.length == 3)
				{
					config.learner_ip = data[1];
					config.learner_port = Integer.parseInt(data[2]);
				} else if (data[0].equals("batching") && data.length == 2)
				{
					config.batching = Boolean.parseBoolean(data[1]);
				} else if (data[0].equals("batchsize") && data.length == 2)
				{
					config.batchsize = Integer.parseInt(data[1]);
				} else if (data[0].equals("catchUpBatchsize") && data.length == 2)
				{
					config.catchUpBatchsize = Integer.parseInt(data[1]);
				} else if (data[0].equals("bufferMultiplier") && data.length == 2)
				{
					config.bufferMultiplier = Integer.parseInt(data[1]);
				} else if (data[0].equals("socketTimeout") && data.length == 2)
				{
					config.socketTimeout = Boolean.parseBoolean(data[1]);
				} else if (data[0].equals("tTimeout") && data.length == 2)
				{
					config.tTimeout = Integer.parseInt(data[1]);
				} else if (data[0].equals("sleep") && data.length == 2)
				{
					config.sleep = Boolean.parseBoolean(data[1]);
				} else if (data[0].equals("tSleep") && data.length == 2)
				{
					config.tSleep = Integer.parseInt(data[1]);
				} else if (data[0].equals("majority") && data.length == 2)
				{
					config.majority = Integer.parseInt(data[1]);
				} else if (data[0].equals("acceptorTimeout") && data.length == 2)
				{
					config.acceptorTimeout = Integer.parseInt(data[1]);
				} else if (data[0].equals("proposerTimeout") && data.length == 2)
				{
					config.proposerTimeout = Integer.parseInt(data[1]);
				} else if (data[0].equals("learnerCatchUpTimer") && data.length == 2)
				{
					config.learnerCatchUpTimer = Integer.parseInt(data[1]);
				}else if (data[0].equals("proposerCatchUp") && data.length == 2)
				{
					config.proposerCatchUp = Boolean.parseBoolean(data[1]);
				} else
				{
					System.out.println(data[0]);
					System.err.println("config file use wrong format2");
				}

				
			}
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return config;
	}

	/**
	 * 
	 * @param m
	 * @param s
	 * @param targetgroup
	 * @param target_port
	 * @throws IOException
	 * @throws InterruptedException
	 */

	public static void send(Message m, MulticastSocket s, String targetgroup, int target_port)
			throws IOException, InterruptedException
	{
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		ObjectOutputStream out = null;

		out = new ObjectOutputStream(new BufferedOutputStream(byteStream));
		out.flush();
		out.writeObject(m);
		out.flush();

		byte[] sendBuf = byteStream.toByteArray();

		DatagramPacket packet = new DatagramPacket(sendBuf, sendBuf.length, InetAddress.getByName(targetgroup),
				target_port);

		s.send(packet);

		// sending to fast causes message loss with udp multicast, on multiple machines
		// should be avoidable, the bigger the number of paxos partecipants running
		// locally
		// the bigger the sleep will have to be (overflow multicast buffersize...)

		// or increase

		// s.setReceiveBufferSize(size);
		// s.setSendBufferSize(size);

		// or both
		if (config.sleep)
		{
			Thread.sleep(config.tSleep);
		}
	}

	/**
	 * 
	 * @param s
	 * @param addres
	 * @param port
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	public static Object recieve(MulticastSocket s, String addres, int port)
			throws IOException, ClassNotFoundException, InterruptedException
	{

		// Random calculations
		//
		// int size = s.getReceiveBufferSize();
		// byte[] recvBuf = new byte[size];
		// size of Value in byte[116]
		// with batchsize =1 largest msg byte[301] -> 320
		// 320 - 116 = 204 //message with no values -> 256
		// 256+116*size
		// 100 -> 11'856 but 3100
		// 3100/100 = 31
		// 256 + 31 =

		// TODO if something breaks look into this
		byte[] recvBuf = new byte[300 + 32 * config.batchsize];

		DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length, InetAddress.getByName(addres), port);
		try
		{
			s.receive(packet);
		} catch (java.net.SocketTimeoutException e)
		{
			return null;
		}

		ByteArrayInputStream byteStream = new ByteArrayInputStream(recvBuf);
		ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(byteStream));
		Object o = is.readObject();
		return o;
	}

}
