package Util;

public class Config
{
	public int id;

	public int client_port;
	public int proposer_port;
	public int acceptors_port;
	public int learner_port;

	public String client_ip;
	public String proposer_ip;
	public String acceptors_ip;
	public String learner_ip;

	public boolean batching = false;
	public int batchsize = 30;

	public int catchUpBatchsize = 10;

	public int bufferMultiplier = 1;

	public boolean socketTimeout = false;
	public int tTimeout = 1000;

	public boolean sleep = true;
	public int tSleep = 1;

	public int majority = 2;

	public int acceptorTimeout = 1000;
	public int proposerTimeout = 10000;
	public int learnerCatchUpTimer = 100;
	
	public boolean proposerCatchUp = false;
	
}
