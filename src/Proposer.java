import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import Messages.ClientCatchUp;
import Messages.ClientMessage;
import Messages.KillMessage;
import Messages.Message1A;
import Messages.Message1B;
import Messages.Message2A;
import Messages.Message2B;
import Messages.Message3;
import Messages.MessageCatchUp;
import Messages.WrongPaxosRnd;
import Util.Config;
import Util.Util;
import Util.Value;

/**
 * @author chris
 *
 */
public class Proposer
{

	// timer shared variables
	public static boolean catchup = false;
	public static Map<Integer, Integer> clients_expexted_id = new HashMap<>();
	public static Map<Integer, Boolean> clients_to_catch = new HashMap<>();

	// variables shared with timer
	// paxos phase
	private static int phase = 0;
	// round
	private static int rnd = 0;
	// reset to phase 0 trigger
	private static boolean toreset = true;

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException
	{
		// setup system for IPv4
		System.setProperty("java.net.preferIPv4Stack", "true");

		// read configs
		Config config = Util.getConfig(args);
		int id = config.id;
		String ip = config.proposer_ip;
		int port = config.proposer_port;

		// stored decisions
		Map<Integer, List<Value>> decisions = new HashMap<>();

		// // decided values
		// List<Value> decided = new ArrayList<>();

		// pending values, sent to decide, but not yet approved
		List<Value> pending = new ArrayList<>();

		// values from clients, to propose
		List<Value> propose = new ArrayList<>();
		Map<Integer, List<Value>> waiting = new HashMap<>();

		// init multicast sockets
		MulticastSocket s;
		s = new MulticastSocket(port);
		s.joinGroup(InetAddress.getByName(ip));
		s.setReceiveBufferSize(s.getReceiveBufferSize() * config.bufferMultiplier);
		s.setSendBufferSize(s.getSendBufferSize() * config.bufferMultiplier);
		if (config.socketTimeout)
		{
			s.setSoTimeout(config.tTimeout);
		}

		// paxos variables
		int paxos_rnd = 0;
		// int rnd = 0;

		// count to check majority
		int count = 0;

		// paxos phase tracker
		// int phase = 0;

		// id's of votes
		List<Integer> votes = new ArrayList<>();

		// values to decide for current round
		List<Value> val = new ArrayList<>();

		// maximum 1B round received
		int maxrnd = -1;

		// timer to reset in case stuck waiting for majority
		Timer timer = new Timer();
		TimerTask myTask = new TimerTask()
		{
			@Override
			public void run()
			{
				if (toreset)
				{
					phase = 0;
					rnd++;
				}
			}
		};

		// change according to latency of the distributed system in ms
		timer.scheduleAtFixedRate(myTask, 0, config.proposerTimeout);

		Timer timer2 = new Timer();
		TimerTask myTask2 = new TimerTask()
		{
			@Override
			public void run()
			{
				if (catchup)
				{
					catchup = false;
					try
					{
						for (Integer clientID : clients_to_catch.keySet())
						{
							if (clients_to_catch.get(clientID))
							{
								clients_to_catch.put(clientID, false);
								ClientCatchUp c = new ClientCatchUp(clients_expexted_id.get(clientID));
								Util.send(c, s, config.learner_ip, config.learner_port);
							}
						}

					} catch (IOException | InterruptedException e)
					{
						e.printStackTrace();
					}
				}

			}
		};

		if (config.proposerCatchUp)
		{
			// change according to how ofet you want to check if need to catch up
			timer2.scheduleAtFixedRate(myTask2, 0, config.learnerCatchUpTimer);
		}

		// main loop
		while (true)
		{

			// recieve message
			Object o = Util.recieve(s, ip, port);

			/**
			 * 
			 * count majority of 1B
			 * 
			 */
			if (Message1B.class.isInstance(o))
			{
				Message1B m = (Message1B) o;
				if (phase == 1)
				{
					// if new 1B to me, for this round, count
					if (m.toId == id && m.rnd == rnd && m.paxos_rnd == paxos_rnd && !votes.contains(m.fromId))
					{
						// count vote
						count++;
						// add voter id
						votes.add(m.fromId);

						// if voter previous decisions are the latest, save them
						if (m.vrnd > maxrnd)
						{
							val = m.values;
							maxrnd = m.vrnd;
						}
					}

					// if majority reached go to next phase
					if (count >= config.majority)
					{
						toreset = false;
						phase++;
					}
				}
			}

			/**
			 * 
			 * get majority of 2B
			 * 
			 */
			else if (Message2B.class.isInstance(o))
			{
				Message2B m = (Message2B) o;

				if (phase == 5)
				{
					// if new 2B to me, for this round, count
					if (m.toID == id && m.rnd == rnd && m.paxos_rnd == paxos_rnd && !votes.contains(m.fromId))
					{
						// count vote
						count++;
						// add voter id
						votes.add(m.fromId);
					}

					// if majority reached go to next phase
					if (count >= config.majority)
					{
						toreset = false;
						phase++;
					}
				}
			}

			/**
			 * 
			 * Message with values from client
			 * 
			 */
			else if (ClientMessage.class.isInstance(o))
			{
				ClientMessage m = (ClientMessage) o;
				Value value = m.value;
				Integer expected = clients_expexted_id.get(value.clientId);
				// if expected sequence from client or catchup disabled
				if (expected == null || expected == value.seq_num || !config.proposerCatchUp)
				{
					propose.add(value);
					clients_expexted_id.put(value.clientId, value.seq_num + 1);
					List<Value> torem = new ArrayList<>();
					if (!waiting.containsKey(value.clientId))
					{
						waiting.put(value.clientId, new ArrayList<>());
					}
					for (Value v : waiting.get(value.clientId))
					{
						expected = clients_expexted_id.get(value.clientId);
						if (expected == null || expected == value.seq_num)
						{
							if (!propose.contains(v) && !pending.contains(v) && !val.contains(v))
							{
								propose.add(v);
							}
							torem.add(v);
							clients_expexted_id.put(v.clientId, v.seq_num + 1);
						}
					}
					waiting.get(value.clientId).removeAll(torem);
				} else
				{
					clients_to_catch.put(value.clientId, true);
					catchup = true;
					if (!waiting.containsKey(value.clientId))
					{
						waiting.put(value.clientId, new ArrayList<>());
					}
					waiting.get(value.clientId).add(value);
				}

			}

			/**
			 * 
			 * decision message, add values if not already added and move to next paxos rnd
			 * 
			 */
			else if (Message3.class.isInstance(o))
			{
				Message3 m = (Message3) o;

				// not my decision
				if (m.fromId != id)
				{
					List<Value> valtmp = new ArrayList<>(m.decision);
					// not already decided
					if (!decisions.containsKey(m.paxos_rnd))
					{
						// decided.addAll(new ArrayList<>(m.decision));
						decisions.put(paxos_rnd, valtmp);
					}

					// remove

					pending.removeAll(valtmp);
					propose.removeAll(valtmp);
					val.removeAll(valtmp);

					// catch up to latest paxos round
					if (m.paxos_rnd >= paxos_rnd)
					{
						paxos_rnd = m.paxos_rnd + 1;
						rnd = 0;
						phase = 0;
					}
				}
			}

			/**
			 * 
			 * sent acceptors old paxos round, answer from acceptors to catch up
			 * 
			 */
			else if (WrongPaxosRnd.class.isInstance(o))
			{
				WrongPaxosRnd m = (WrongPaxosRnd) o;
				if (paxos_rnd < m.paxos_rnd)
				{
					paxos_rnd = m.paxos_rnd;
					rnd = m.rnd;
					phase = 0;
				}
			} else if (KillMessage.class.isInstance(o))
			{
				timer.cancel();
				return;
			}else if (MessageCatchUp.class.isInstance(o))
			{
				MessageCatchUp m = (MessageCatchUp) o;
				// not from me
				if (m.fromId != id)
				{
					int i = m.paxos_rnd;
					int countt = 0;
					List<Value> v = decisions.get(i);
					// send only 10. If more needed the learner will ask again
					while (v != null && countt < config.catchUpBatchsize)
					{
						Message3 ans = new Message3(v, id, -8, i);
						Util.send(ans, s, config.learner_ip, config.learner_port);
						i++;
						countt++;
						v = decisions.get(i);
					}
				}
			}

			/**
			 * 
			 * Ask for majority of acceptors to listen to my value, send 1A
			 * 
			 */
			if (phase == 0)
			{
				Message1A m = new Message1A(id, rnd, paxos_rnd);
				Util.send(m, s, config.acceptors_ip, config.acceptors_port);

				// reset majority counter and mav round for decided values
				count = 0;
				val.clear();
				maxrnd = Integer.MIN_VALUE;
				votes.clear();

				// enable timer reset
				toreset = true;

				phase++;
			}

			/**
			 * 
			 * received majority of 1B, check if there are already decision
			 * 
			 */
			if (phase == 2)
			{
				// there are no decision made previously
				if (val.isEmpty())
				{
					phase++;
				}
				// there are decision made previously
				else
				{
					pending.addAll(val);
					phase += 2;
				}
			}

			/**
			 * 
			 * No previous decision, when there is something to propose make decision set
			 * 
			 */
			if (phase == 3)
			{
				pending.addAll(propose);
				propose.clear();
				if (pending.size() > config.batchsize)
				{
					val.addAll(pending.subList(0, config.batchsize));
					phase++;
				} else if (!config.batching)
				{
					val.addAll(pending);
					if (!val.isEmpty())
					{
						phase++;
					}
				} else if (config.batching && pending.size() == config.batchsize)
				{
					val.addAll(pending);
					phase++;
				}
			}

			/**
			 * 
			 * send decisions in 2A
			 * 
			 */
			if (phase == 4)
			{
				// prepare and send 2A
				Message2A ans = new Message2A(val, id, rnd, paxos_rnd);
				Util.send(ans, s, config.acceptors_ip, config.acceptors_port);

				// reset votes for 2B
				count = 0;
				votes.clear();

				// enable timeout for 2B
				toreset = true;

				phase++;
			}

			/**
			 * 
			 * received majority of 2B decide and send to learner and others
			 * 
			 */
			if (phase == 6)
			{
				// add decided values
				// decided.addAll(val);

				// prepare message 3 with decision
				Message3 ans = new Message3(val, id, rnd, paxos_rnd);
				Util.send(ans, s, config.acceptors_ip, config.acceptors_port);
				Util.send(ans, s, config.learner_ip, config.learner_port);
				Util.send(ans, s, config.proposer_ip, config.proposer_port);

				// copy val, otherwise it becomes empty list
				decisions.put(paxos_rnd, new ArrayList<>(val));

				// remove decided values from pending
				pending.removeAll(new ArrayList<>(val));

				// go to phase 0 next paxos round
				phase = 0;
				rnd = 0;
				paxos_rnd++;
			}

		}
	}
}
