import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.List;

import Util.Config;
import Util.Util;

public class StatClient
{
	//Not finished
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException
	{
		// setup system for IPv4
		System.setProperty("java.net.preferIPv4Stack", "true");

		// read configs
		Config config = Util.getConfig(args);
		int id = config.id;
		String ip = config.client_ip;
		int port = config.client_port;

		// sequence number for sent values, to differentiate them
		long seq_num = 0;

		// set uo multicast socket
		MulticastSocket s = new MulticastSocket(port);
		s.joinGroup(InetAddress.getByName(ip));
		s.setSendBufferSize(s.getSendBufferSize() * config.bufferMultiplier);

		// start a proposer
		Thread.sleep(1000);
		Thread t1 = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				try
				{
					Proposer.main(args);
				} catch (ClassNotFoundException | IOException | InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		});
		t1.start();

		// start a acceptor1
		Thread.sleep(1000);
		Thread t2 = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				try
				{
					Acceptor.main(args);
				} catch (ClassNotFoundException | IOException | InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		});
		t2.start();

		// start a acceptor2
		Thread.sleep(1000);
		args[0] = "99";
		Thread t3 = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				try
				{
					Acceptor.main(args);
				} catch (ClassNotFoundException | IOException | InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		});
		t3.start();

		// start a learner
		Thread.sleep(1000);
		Thread t4 = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					LearnersStat.main(args);
				} catch (ClassNotFoundException | IOException | InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		});
		t4.start();
		
		
		int numClients = Integer.parseInt(args[3]);
		List<Thread> clients = new ArrayList<>();
		Thread.sleep(1000);
		args[0] = "120";
		for (int i = 0; i < numClients; i++)
		{
			Thread.sleep(1000);
			args[0] = new Integer(i+120).toString();
			clients.add(new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						ClientStat.main(args);
					} catch (ClassNotFoundException | IOException | InterruptedException e)
					{
						e.printStackTrace();
					}	
				}
			}));
		}
		
		//not simultaneous but close enough
		System.out.print(""+numClients +","+ args[2] + "," + System.nanoTime() + ",");
		for(Thread client : clients) {
			client.start();
		}
		System.out.print( System.nanoTime() + ",");

		

	}

}
