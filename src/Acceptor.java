import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import Messages.KillMessage;
import Messages.Message1A;
import Messages.Message1B;
import Messages.Message2A;
import Messages.Message2B;
import Messages.Message3;
import Messages.MessageCatchUp;
import Messages.WrongPaxosRnd;
import Util.Config;
import Util.Util;
import Util.Value;

/**
 * 
 */

/**
 * @author Christian Vuerich
 *
 */
public class Acceptor
{
	// timer shared variables
	private static boolean reset = true;
	private static boolean toreset = true;

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException
	{

		// setup system for IPv4
		System.setProperty("java.net.preferIPv4Stack", "true");

		// stored decisions
		Map<Integer, List<Value>> decisions = new HashMap<>();

		// read configs
		Config config = Util.getConfig(args);
		int id = config.id;
		String ip = config.acceptors_ip;
		int port = config.acceptors_port;

		// init paxos variables
		int paxos_rnd = 0;
		int rnd = -1;
		int vrnd = -1;
		int leader = Integer.MAX_VALUE;

		// init multicast sockets
		MulticastSocket s;
		s = new MulticastSocket(port);
		s.joinGroup(InetAddress.getByName(ip));
		s.setReceiveBufferSize(s.getReceiveBufferSize() * config.bufferMultiplier);
		s.setSendBufferSize(s.getSendBufferSize() * config.bufferMultiplier);
		if (config.socketTimeout)
		{
			s.setSoTimeout(config.tTimeout);
		}

		// kill current leader in case is dead
		Timer timer = new Timer();
		TimerTask myTask = new TimerTask()
		{
			@Override
			public void run()
			{
				if (toreset)
				{
					reset = true;
				}
				toreset = true;
			}
		};

		// change according to latency of the distributed system in ms
		timer.scheduleAtFixedRate(myTask, 0, config.acceptorTimeout);

		// main loop
		while (true)
		{

			// read message
			Object o = Util.recieve(s, ip, port);

			/**
			 * 
			 * receive 1A if no temporary leader choose smaller id, send decided values if
			 * any
			 * 
			 */
			if (Message1A.class.isInstance(o))
			{
				Message1A m = (Message1A) o;

				// if new round and prop id less than leader or suspected/timedout leader
				if (m.paxos_rnd >= paxos_rnd && m.rnd > rnd && (m.fromId <= leader || reset))
				{
					reset = false;
					// if leader keep asking 1A override killing timer
					toreset = false;
					leader = m.fromId;
					paxos_rnd = m.paxos_rnd;
					rnd = m.rnd;
					Message1B ans = new Message1B(decisions.get(m.paxos_rnd), id, m.fromId, rnd, vrnd, paxos_rnd);
					Util.send(ans, s, config.proposer_ip, config.proposer_port);
				}

				// old instance of paxos proposer, send a catch up message
				if ((m.paxos_rnd < paxos_rnd || m.rnd <= rnd) && (reset || m.fromId == leader))
				{
					WrongPaxosRnd mess = new WrongPaxosRnd(id, paxos_rnd, rnd + 1);
					Util.send(mess, s, config.proposer_ip, config.proposer_port);
				}
			}

			/**
			 * 
			 * receive Proposal 2A
			 * 
			 */
			else if (Message2A.class.isInstance(o))
			{
				Message2A m = (Message2A) o;
				// if for current round and from leader
				if (m.paxos_rnd == paxos_rnd && m.rnd == rnd && m.fromId == leader)
				{
					paxos_rnd = m.paxos_rnd;
					rnd = m.rnd;
					decisions.put(paxos_rnd, m.decision);
					vrnd = rnd;
					Message2B ans = new Message2B(m.decision, id, m.fromId, rnd, paxos_rnd);
					Util.send(ans, s, config.proposer_ip, config.proposer_port);
				}
			}

			/**
			 * 
			 * receive decisions 3
			 * 
			 */
			else if (Message3.class.isInstance(o))
			{
				Message3 m = (Message3) o;
				// if not decided yet decide
				if (!decisions.containsKey(m.paxos_rnd))
				{
					decisions.put(m.paxos_rnd, m.decision);
				}
				// catch up to latest paxos rnd
				if (m.paxos_rnd >= paxos_rnd)
				{
					paxos_rnd = m.paxos_rnd + 1;
					vrnd = -1;
					rnd = -1;
				}
			}

			/**
			 * 
			 * request from learner for values
			 * 
			 */
			else if (MessageCatchUp.class.isInstance(o))
			{
				MessageCatchUp m = (MessageCatchUp) o;
				int i = m.paxos_rnd;
				int count = 0;
				List<Value> v = decisions.get(i);
				// send max 10 values, if more needed learner will ask again
				while (v != null && count < config.catchUpBatchsize)
				{
					Message3 ans = new Message3(v, id, -8, i);
					Util.send(ans, s, config.learner_ip, config.learner_port);
					i++;
					count++;
					v = decisions.get(i);
				}
			} else if (KillMessage.class.isInstance(o))
			{
				timer.cancel();
				return;
			}
		}
	}
}
