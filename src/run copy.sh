#!/usr/bin/env bash

projdir="$1"
conf=`pwd`/paxos.conf
n="$2"

if [[ x$projdir == "x" || x$n == "x" ]]; then
	echo "Usage: $0 <project dir> <number of values per proposer>"
    exit 1
fi

# following line kills processes that have the config file in its cmdline
KILLCMD="pkill -f $conf"

$KILLCMD

cd $projdir

../generate.sh $n > ../prop1
../generate.sh $n > ../prop2
../generate.sh $n > ../prop3
../generate.sh $n > ../prop4
../generate.sh $n > ../prop5
../generate.sh $n > ../prop6

echo "starting acceptors..."

./acceptor.sh 1 $conf &
# ./acceptor.sh 2 $conf &
# ./acceptor.sh 1 $conf &
./acceptor.sh 2 $conf &
./acceptor.sh 3 $conf &
# ./acceptor.sh 4 $conf &
# ./acceptor.sh 5 $conf &
# ./acceptor.sh 6 $conf &
# ./acceptor.sh 7 $conf &
# ./acceptor.sh 8 $conf &
# ./acceptor.sh 9 $conf &
# ./acceptor.sh 10 $conf &
# ./acceptor.sh 11 $conf &
# ./acceptor.sh 12 $conf &

sleep 1
echo "starting learners..."

./learner.sh 1 $conf > ../learn1 &
./learner.sh 2 $conf > ../learn2 &

sleep 1
echo "starting proposers..."

./proposer.sh 1 $conf &
./proposer.sh 2 $conf &
./proposer.sh 3 $conf &
./proposer.sh 4 $conf &
./proposer.sh 5 $conf &
./proposer.sh 6 $conf &
./proposer.sh 7 $conf &
./proposer.sh 8 $conf &
./proposer.sh 9 $conf &
./proposer.sh 10 $conf &
./proposer.sh 11 $conf &
./proposer.sh 12 $conf &
./proposer.sh 13 $conf &
./proposer.sh 14 $conf &
./proposer.sh 15 $conf &


echo "waiting to start clients"
sleep 10
echo "starting clients..."

./client.sh 1 $conf < ../prop1 &
./client.sh 2 $conf < ../prop2 &
# ./client.sh 3 $conf < ../prop3 &
# ./client.sh 4 $conf < ../prop4 &
# ./client.sh 5 $conf < ../prop5 &
# ./client.sh 6 $conf < ../prop6 &


sleep "$3"

$KILLCMD
wait

cd ..
