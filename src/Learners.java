import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import Messages.KillMessage;
import Messages.Message3;
import Messages.MessageCatchUp;
import Util.Config;
import Util.Util;
import Util.Value;

/**
 * @author Christian Vuerich
 *
 */
public class Learners
{

	// timer shared variables
	public static boolean catchup = false;
	public static int paxos_rnd = 0;
	public static int lastaskedto = -1;

	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException
	{
		// setup system for IPv4
		System.setProperty("java.net.preferIPv4Stack", "true");

		// read configs
		Config config = Util.getConfig(args);
		int id = config.id;
		String ip = config.learner_ip;
		int port = config.learner_port;

		// stored decisions
		Map<Integer, List<Value>> decisions = new HashMap<>();

		// ahead decision to learn later
		List<Message3> m3 = new ArrayList<>();
		// ConcurrentLinkedQueue<Message3> m3 = new ConcurrentLinkedQueue<>();

		// tmp trash for processed messages
		List<Message3> r = new ArrayList<>();

		// init multicast sockets
		MulticastSocket s;
		s = new MulticastSocket(port);
		s.joinGroup(InetAddress.getByName(ip));
		s.setReceiveBufferSize(s.getReceiveBufferSize() * config.bufferMultiplier);
		s.setSendBufferSize(s.getSendBufferSize() * config.bufferMultiplier);
		if (config.socketTimeout)
		{
			s.setSoTimeout(config.tTimeout);
		}

		// catch up timer, if behind on decision, ask for old decisions
		Timer timer = new Timer();
		TimerTask myTask = new TimerTask()
		{
			@Override
			public void run()
			{
				if (catchup)
				{
					catchup = false;
					MessageCatchUp p = new MessageCatchUp(id, paxos_rnd);
					try
					{
						// first ask other learner
						if (lastaskedto != paxos_rnd)
						{
							Util.send(p, s, config.learner_ip, config.learner_port);
							lastaskedto = paxos_rnd;
						}
						// if no answer ask acceptors
						else
						{
							Util.send(p, s, config.acceptors_ip, config.acceptors_port);
						}
					} catch (IOException | InterruptedException e)
					{
						e.printStackTrace();
					}
				}

			}
		};

		// change according to how ofet you want to check if need to catch up
		timer.scheduleAtFixedRate(myTask, 0, config.learnerCatchUpTimer);

		// main loop
		while (true)
		{
			// read message
			Object o = Util.recieve(s, ip, port);

			// if decision message
			if (Message3.class.isInstance(o))
			{
				Message3 m = (Message3) o;

				// for current round, decide
				if (m.paxos_rnd == paxos_rnd)
				{
					paxos_rnd++;

					if (!decisions.containsKey(m.paxos_rnd))
					{
						decisions.put(m.paxos_rnd, m.decision);
						for (Value v : m.decision)
						{
							System.out.println(v.value);
						}
					}
				}
				// catch up answers for future round, store it for later
				else if (m.rnd < 0 && m.paxos_rnd > paxos_rnd)
				{
					m3.add(m);
				}
				// missed something, enable catch up and store message for later
				else if (m.paxos_rnd > paxos_rnd)
				{
					m3.add(m);
					catchup = true;
				}
			}

			/**
			 * 
			 * request from learner to catch up
			 * 
			 */
			else if (MessageCatchUp.class.isInstance(o))
			{
				MessageCatchUp m = (MessageCatchUp) o;
				// not from me
				if (m.fromId != id)
				{
					int i = m.paxos_rnd;
					int count = 0;
					List<Value> v = decisions.get(i);
					// send only 10. If more needed the learner will ask again
					while (v != null && count < config.catchUpBatchsize)
					{
						Message3 ans = new Message3(v, id, -8, i);
						Util.send(ans, s, config.learner_ip, config.learner_port);
						i++;
						count++;
						v = decisions.get(i);
					}
				}
			} else if (KillMessage.class.isInstance(o))
			{
				timer.cancel();
				return;
			}

			/**
			 * 
			 * sort maybe not needed, but guarantees efficient learning
			 * 
			 */
			r.clear();
			// Collections.sort(m3);
			for (Message3 m : m3)
			{
				// for current round, decide
				if (m.paxos_rnd == paxos_rnd)
				{
					paxos_rnd++;

					if (!decisions.containsKey(m.paxos_rnd))
					{
						decisions.put(m.paxos_rnd, m.decision);
						for (Value v : m.decision)
						{
								System.out.println(v.value);
						}
					}
				}
				// if learned message or old message put into trash
				if (m.paxos_rnd <= paxos_rnd)
				{
					r.add(m);
				}
			}
			// delete processed messages
			m3.removeAll(r);
		}
	}
}
