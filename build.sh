#!/usr/bin/env bash

mkdir -p out

javac -d out/  \
    ./src/*.java \
    ./src/Messages/*.java \
    ./src/Util/*.java

cp ./src/*.sh ./out